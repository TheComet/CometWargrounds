package net.nyxcraft.dev.kitpvp.util;

import net.nyxcraft.dev.kitpvp.game.Kit;
import net.nyxcraft.dev.nyxcore.ui.Menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class KitMenu extends Menu {

    private static final Material SELECT = Material.DIAMOND;
    private static final Material EDIT = Material.ANVIL;
    private static final Material DELETE = Material.SPONGE;
    private static final Material DEFAULT = Material.STAINED_CLAY;
    private static final int[] KIT_INDICES = { 0, 2, 4, 6, 8 };
    
    private final Kit kit;
    
    public KitMenu(Player player, Kit kit) {
        super("Kit Selection", 6);
        
        this.kit = kit == null ? new Kit() : kit;
        
        this.populateMenu(player);
        this.openMenu(player);
    }
    
    private void populateMenu(Player player) {
        for (int i : KIT_INDICES) {
            populateColumn(player, i);
        }
    }
    
    private void populateColumn(Player player, int column) {
        
    }
    
    private Items getItem(Player player, Items.ItemType type, int index) {
        return null;
    }
    
    
    private static ItemStack coalesce(Items item) {
        ItemStack itemstack = item == null ? null : item.get();
        return itemstack != null ? itemstack : ItemStackUtil.build(DEFAULT).withDisplay(item == null ? " " : item.getDisplay()).build();
    }
}
