package net.nyxcraft.dev.kitpvp.util;

import java.util.Random;
import java.util.function.Supplier;

import net.nyxcraft.dev.kitpvp.game.progression.ProgressionItem;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @deprecated In favor of {@link ProgressionItem} and the progression system.
 */
public interface Items {

    /**
     * Get the display name of this item.
     * 
     * @return A name for this item.
     */
    public String getDisplay();
    
    /**
     * Get the ItemType of this item.
     * 
     * @return The ItemType for this item.
     */
    public ItemType getItemType();
    
    /**
     * Get an ItemStack representation of this item.
     * <p>
     * If this item has no ItemStack representation, null may be returned.
     * 
     * @return An ItemStack representing this item.
     */
    public ItemStack get();
    
    /**
     * Equip this item for the specified Player.
     * 
     * @param player The Player to equip with this item.
     */
    public void equip(Player player);
    
    /**
     * Unequip this item from the specified Player.
     * 
     * @param player The Player to unequip this item from.
     */
    public void unequip(Player player);
    
    
    public static <I extends Enum<I> & Items> I random(Class<I> cls) {
        I[] items = cls.getEnumConstants();
        return items == null ? null : items[ItemType.random.nextInt(items.length)];
    }
    
    public static enum ItemType {
        PRIMARY,
        SECONDARY,
        SPECIAL,
        PERK;
        
        // Some static fields because they'd have to be public if defined directly in the interface
        private static final Random random = new Random();
        private static final Supplier<ItemStack> ARROWS = ItemStackUtil.build(Material.ARROW, 0, 32).supplier();
    }
    
    public static enum Primary implements Items {
    
        /*
         * Primary Weapons:
         *   Iron Sword:
         *     Fire Aspect 1
         *     Knockback 2
         *     Sharpness 1
         *   Bow:
         *     Flame 1
         *     Punch 1
         *     Power 1
         */
        
        SWORD_FIREASPECT("Flame Sword", ItemStackUtil.build(Material.IRON_SWORD).withEnchant(Enchantment.FIRE_ASPECT, 1).supplier()),
        SWORD_KNOCKBACK("Knockback Sword", ItemStackUtil.build(Material.IRON_SWORD).withEnchant(Enchantment.KNOCKBACK, 2).supplier()),
        SWORD_SHARPNESS("Sharp Sword", ItemStackUtil.build(Material.IRON_SWORD).withEnchant(Enchantment.DAMAGE_ALL, 1).supplier()),
        BOW_FLAME("Flame Bow", ItemStackUtil.build(Material.BOW).withEnchant(Enchantment.ARROW_FIRE, 1).supplier(), ItemType.ARROWS),
        BOW_PUNCH("Punch Bow", ItemStackUtil.build(Material.BOW).withEnchant(Enchantment.ARROW_KNOCKBACK, 1).supplier(), ItemType.ARROWS),
        BOW_POWER("Power Bow", ItemStackUtil.build(Material.BOW).withEnchant(Enchantment.ARROW_DAMAGE, 1).supplier(), ItemType.ARROWS);
        
        private final Supplier<ItemStack> item, extra;
        private final String display;
        
        private Primary(String display, Supplier<ItemStack> item, Supplier<ItemStack> extra) {
            this.display = display;
            this.item = item;
            this.extra = extra;
        }
        
        private Primary(String display, Supplier<ItemStack> item) {
            this.display = display;
            this.item = item;
            this.extra = null;
        }
        
        public String getDisplay() {
            return display;
        }
        
        public ItemType getItemType() {
            return ItemType.PRIMARY;
        }
        
        public ItemStack get() {
            if (item != null) {
                return item.get();
            }
            
            return new ItemStack(Material.AIR);
        }
        
        public void equip(Player player) {
            ItemStack item = get();
            
            if (item != null) {
                player.getInventory().addItem(item);
            }
            
            if (extra != null) {
                ItemStack extra = this.extra.get();
                
                if (extra != null) {
                    player.getInventory().addItem(extra);
                }
            }
        }
        
        public void unequip(Player player) {
            ItemStack item = get();
            
            if (item != null) {
                player.getInventory().all(item.getType())
                        .keySet()
                        .stream()
                        .filter(index -> item.isSimilar(player.getInventory().getItem(index)))
                        .forEachOrdered(player.getInventory()::clear);
            }
        }
    }
    
    public static enum Secondary implements Items {
    
        INSTAKILL("K.O Sword", ItemStackUtil.build(Material.GOLD_SWORD).withEnchant(Enchantment.DAMAGE_ALL, 10).withDurability(58).supplier()),
        BOW("Wooden Bow", ItemStackUtil.build(Material.BOW).supplier(), ItemType.ARROWS),
        SWORD("Wooden Sword", ItemStackUtil.build(Material.WOOD_SWORD).supplier());
        
        private final Supplier<ItemStack> item, extra;
        private final String display;
        
        private Secondary(String display, Supplier<ItemStack> item, Supplier<ItemStack> extra) {
            this.display = display;
            this.item = item;
            this.extra = extra;
        }
        
        private Secondary(String display, Supplier<ItemStack> item) {
            this.display = display;
            this.item = item;
            this.extra = null;
        }
        
        public String getDisplay() {
            return display;
        }
        
        public ItemType getItemType() {
            return ItemType.SECONDARY;
        }
        
        public ItemStack get() {
            if (item != null) {
                return item.get();
            }
            
            return new ItemStack(Material.AIR);
        }
        
        public void equip(Player player) {
            ItemStack item = get();
            
            if (item != null) {
                player.getInventory().addItem(item);
            }
            
            if (extra != null) {
                ItemStack extra = this.extra.get();
                
                if (extra != null) {
                    player.getInventory().addItem(extra);
                }
            }
        }
        
        public void unequip(Player player) {
            ItemStack item = get();
            
            if (item != null) {
                player.getInventory().all(item.getType())
                        .keySet()
                        .stream()
                        .filter(index -> item.isSimilar(player.getInventory().getItem(index)))
                        .forEachOrdered(player.getInventory()::clear);
            }
        }
    }
    
    public static enum Special implements Items {
    
        GOLDEN_APPLE("Golden Apple", ItemStackUtil.build(Material.GOLDEN_APPLE).supplier()),
        POTION_HEALTH("Restoration", ItemStackUtil.build(Material.POTION).withDurability(8229).supplier()),
        POTION_INVISIBILITY("Invisibility", ItemStackUtil.build(Material.POTION).withDurability(8270).supplier()),
        POTION_DAMAGE("Damage", ItemStackUtil.build(Material.POTION).withDurability(16396).supplier()),
        POTION_POISON("Poison", ItemStackUtil.build(Material.POTION).withDurability(16388).supplier()),
        TNT("Blast Charge", ItemStackUtil.build(Material.TNT).supplier());
        
        private final Supplier<ItemStack> item;
        private final String display;
        
        private Special(String display, Supplier<ItemStack> item) {
            this.display = display;
            this.item = item;
        }
        
        public String getDisplay() {
            return display;
        }
        
        public ItemType getItemType() {
            return ItemType.SPECIAL;
        }
        
        public ItemStack get() {
            if (item != null) {
                return item.get();
            }
            
            return new ItemStack(Material.AIR);
        }
        
        public void equip(Player player) {
            ItemStack item = get();
            
            if (item != null) {
                player.getInventory().addItem(item);
            }
        }
        
        public void unequip(Player player) {
            ItemStack item = get();
            
            if (item != null) {
                player.getInventory().all(item.getType())
                        .keySet()
                        .stream()
                        .filter(index -> item.isSimilar(player.getInventory().getItem(index)))
                        .forEachOrdered(player.getInventory()::clear);
            }
        }
    }
    
    public static enum Perks implements Items {
    
        SPEED("Speed", PotionEffectType.SPEED),
        JUMP("Jump", PotionEffectType.JUMP),
        STRENGTH("Strength", PotionEffectType.INCREASE_DAMAGE),
        REGEN("Regeneration", PotionEffectType.REGENERATION),
        RESISTANCE("Resistance", PotionEffectType.DAMAGE_RESISTANCE);
        
        private final PotionEffectType type;
        private final String display;
        
        private Perks(String display, PotionEffectType type) {
            this.display = display;
            this.type = type;
        }
        
        public String getDisplay() {
            return display;
        }
        
        public ItemType getItemType() {
            return ItemType.PERK;
        }
        
        public ItemStack get() {
            return null;
        }
        
        public void equip(Player player) {
            if (type != null) {
                player.removePotionEffect(type);
                player.addPotionEffect(new PotionEffect(type, Integer.MAX_VALUE, 0, true), true);
            }
        }
        
        public void unequip(Player player) {
            if (type != null) {
                player.removePotionEffect(type);
            }
        }
    }
}
