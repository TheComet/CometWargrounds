package net.nyxcraft.dev.kitpvp.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

/**
 * Utility class for handling ItemStack manipulation.
 * 
 * @author Gideon
 */
public class ItemStackUtil {

    private ItemStackUtil() {}
    
    
    public static ItemStack modify(ItemStack item, String display, String[] lore, Map<Enchantment, Integer> enchants) {
        ItemMeta meta = item.getItemMeta();
        
        if (meta != null) {
            if (display != null) {
                meta.setDisplayName(color(display));
            }
            
            if (lore != null && lore.length > 0) {
                meta.setLore(Arrays.asList(lore).stream().map(ItemStackUtil::color).collect(Collectors.toList()));
            }
            
            item.setItemMeta(meta);
        }
        
        if (enchants != null) {
            enchants.forEach(item::addUnsafeEnchantment);
        }
        
        return item;
    }
    
    private static String color(String input) {
        return ChatColor.translateAlternateColorCodes('&', input);
    }
    
    public static ItemBuilder build(Material m) {
        return build(m, 0);
    }
    
    public static ItemBuilder build(Material m, int data) {
        return build(m, data, 1);
    }
    
    public static ItemBuilder build(Material m, int data, int amount) {
        return new ItemBuilder(m, (byte) data, amount);
    }
    
    public static class ItemBuilder {
    
        private ItemStack item;
        
        @SuppressWarnings("deprecation")
        private ItemBuilder(Material m, byte data, int amount) {
            this.item = new MaterialData(m, data).toItemStack(amount);
        }
        
        public ItemBuilder withDisplayAndLore(String display, String... lore) {
            this.item = modify(item, display, lore, null);
            return this;
        }
        
        public ItemBuilder withDisplay(String display) {
            this.item = modify(item, display, null, null);
            return this;
        }
        
        public ItemBuilder withLore(String... lore) {
            this.item = modify(item, null, lore, null);
            return this;
        }
        
        public ItemBuilder withEnchant(Enchantment enchant, int level) {
            this.item = modify(item, null, null, Collections.singletonMap(enchant, Integer.valueOf(level)));
            return this;
        }
        
        public ItemBuilder withEnchants(Map<Enchantment, Integer> enchants) {
            this.item = modify(item, null, null, enchants);
            return this;
        }
        
        public ItemBuilder withDurability(int dura) {
            item.setDurability((short) dura);
            return this;
        }
        
        public ItemStack build() {
            return item;
        }
        
        public Supplier<ItemStack> supplier() {
            return () -> item;
        }
    }
}
