package net.nyxcraft.dev.kitpvp.util;

import javax.annotation.CheckForNull;

import net.nyxcraft.dev.kitpvp.game.MapRotation;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * An Event extension designed to be called when the current map changes.
 * 
 * @author Gideon
 */
public class MapChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    
    private final MapRotation.LoadedMap previous, current;
    
    public MapChangeEvent(MapRotation.LoadedMap previous, MapRotation.LoadedMap current) {
        this.previous = previous;
        this.current = current;
    }
    
    @CheckForNull
    public MapRotation.LoadedMap getPrevious() {
        return previous;
    }
    
    public MapRotation.LoadedMap getCurrent() {
        return current;
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public static void call(MapRotation.LoadedMap from, MapRotation.LoadedMap to) {
        Bukkit.getPluginManager().callEvent(new MapChangeEvent(from, to));
    }
}
