package net.nyxcraft.dev.kitpvp.game.progression;

import java.util.Objects;
import java.util.function.IntUnaryOperator;

public enum Type {

    PRIMARY(costs(true, 100, 220, 350, 450)),
    SECONDARY(costs(true, 150, 320));
    
    private final IntUnaryOperator costfunc;
    
    private Type(IntUnaryOperator costfunc) {
        this.costfunc = Objects.requireNonNull(costfunc);
    }
    
    public int getCost(int depth) {
        return costfunc.applyAsInt(depth);
    }
    
    
    private static IntUnaryOperator costs(final boolean clamp, final int... costs) {
        if (costs.length < 1) {
            throw new IllegalArgumentException("The given int[] has no cost values!");
        }
        
        return depth -> {
            if (depth < 0 || depth >= costs.length) {
                if (!clamp) {
                    throw new IllegalArgumentException("The given depth is not valid for this cost function!");
                }
                
                depth = Math.max(0, Math.min(depth, costs.length - 1));
            }
            
            return costs[depth];
        };
    }
}