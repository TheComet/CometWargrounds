package net.nyxcraft.dev.kitpvp.game.progression;

import org.bukkit.entity.Player;

public interface Effect {

    /**
     * Get the technical name of this Effect.
     * <p>
     * Designed to be automatically implemented by {@link Enum#name()}.
     * 
     * @return The technical name of this effect.
     */
    public String name();
    
    /**
     * Get the display name of this Effect, preferably output-friendly.
     * 
     * @return A String to represent this Effect textually.
     */
    public default String getDisplay() {
        String display = name().replaceAll("_", " ").toLowerCase();
        return Character.toUpperCase(display.charAt(0)) + display.substring(1);
    }
    
    /**
     * Test for whether or not the given Player can use this Effect.
     * 
     * @param player The Player to test.
     * @return <b>true</b> if they are allowed to use this.
     */
    public default boolean check(Player player) {
        return true;
    }
    
    /**
     * Equip the given Player, to the extent that the implementation desires.
     * 
     * @param player The Player to equip.
     */
    public void equip(Player player);
    
    /**
     * Unequip the given Player, to the extent that the implementation desires.
     * 
     * @param player The Player to unequip.
     */
    public void unequip(Player player);
}