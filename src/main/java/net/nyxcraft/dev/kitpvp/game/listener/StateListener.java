package net.nyxcraft.dev.kitpvp.game.listener;

import java.util.HashSet;
import java.util.Objects;

import net.nyxcraft.dev.kitpvp.KitPVP;
import net.nyxcraft.dev.kitpvp.game.GameManager;
import net.nyxcraft.dev.kitpvp.game.GameManager.PlayerData;
import net.nyxcraft.dev.kitpvp.game.Kit;
import net.nyxcraft.dev.kitpvp.game.MapRotation;
import net.nyxcraft.dev.kitpvp.game.PlayerState;
import net.nyxcraft.dev.kitpvp.game.SpectatorItems;
import net.nyxcraft.dev.kitpvp.util.PlayerStateChangeEvent;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Listener for {@link PlayerState} and player visibility handling.
 * 
 * @author Gideon
 */
public class StateListener implements Listener  {

    private final HashSet<String> hidden;
    private final MapRotation maps;
    private final GameManager gm;
    
    public StateListener() {
        this.hidden = new HashSet<>();
        this.maps = KitPVP.getInstance().getMapRotation();
        this.gm = GameManager.getInstance();
    }
    
    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent e) {
        Player joined = e.getPlayer();
        
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (joined.equals(player)) {
                continue;
            } else if (hidden.contains(player.getName())) {
                joined.hidePlayer(player);
            } else {
                joined.showPlayer(player);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent e) {
        hidden.remove(e.getPlayer().getName());
    }
    
    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        Entity damaged = e.getEntity();
        
        if (damaged instanceof Player) {
            if (hidden.contains(((HumanEntity) damaged).getName())) {
                e.setCancelled(true);
            } else if (PlayerState.SPECTATING.equals(gm.findPlayerData((Player) damaged))) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onPvE(EntityDamageByBlockEvent e) {
        Entity damaged = e.getEntity();
        
        if (damaged instanceof Player) {
            if (hidden.contains(((HumanEntity) damaged).getName())) {
                e.setCancelled(true);
            } else if (PlayerState.SPECTATING.equals(gm.findPlayerData((Player) damaged))) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onPvP(EntityDamageByEntityEvent e) {
        Entity dd = e.getEntity(), dr = e.getDamager();
        
        if (dd instanceof Player) {
            if (hidden.contains(((HumanEntity) dd).getName())) {
                e.setCancelled(true);
            } else if (PlayerState.SPECTATING.equals(gm.findPlayerData((Player) dd).getState())) {
                e.setCancelled(true);
            }
        }
        
        if (dr instanceof Player) {
            if (hidden.contains(((HumanEntity) dr).getName())) {
                e.setCancelled(true);
            } else if (PlayerState.SPECTATING.equals(gm.findPlayerData((Player) dr).getState())) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onStateChange(PlayerStateChangeEvent e) {
        PlayerData data = e.getData();
        Player player = data.getPlayer();
        
        if (data.isValid() && !Objects.equals(e.getTo(), e.getFrom())) {
            e.getPlayer().setNoDamageTicks(20);
            e.getPlayer().setFireTicks(0);
            e.getPlayer().setFallDistance(0f);
            
            switch (e.getTo()) {
            case SPECTATING:
                spectate(player);
                break;
            case FIGHTING:
                fight(player);
                break;
            }
        }
    }
    
    @SuppressWarnings("deprecation")
    private void spectate(Player player) {
        hidden.add(player.getName());
        
        Kit.unequip(player);
        player.getInventory().clear();
        SpectatorItems.giveTo(player);
        player.setAllowFlight(true);
        player.setFlying(true);
        
        for (Player other : Bukkit.getOnlinePlayers()) {
            if (player.equals(other)) {
                continue;
            }
            
            other.hidePlayer(player);
        }
    }
    
    @SuppressWarnings("deprecation")
    private void fight(Player player) {
        hidden.remove(player.getName());

        PlayerData data = gm.getPlayerData(player);
        player.getInventory().clear();
        player.setFlying(false);
        player.setAllowFlight(false);
        
        MapRotation.LoadedMap current = maps.getCurrentMap();
        
        if (current != null) {
            player.teleport(current.getRandomSpawn());
        }
        
        if (data != null && data.getKit() != null) {
            data.getKit().equip(player);
        }
        
        for (Player other : Bukkit.getOnlinePlayers()) {
            if (player.equals(other)) {
                continue;
            }
            
            other.showPlayer(player);
        }
    }
}
