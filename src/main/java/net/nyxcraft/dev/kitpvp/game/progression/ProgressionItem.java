package net.nyxcraft.dev.kitpvp.game.progression;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import net.nyxcraft.dev.kitpvp.game.GameManager;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Blink;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Charged;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Grapple;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Rage;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Rapid;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Slingshot;
import net.nyxcraft.dev.kitpvp.game.progression.passive.Steady;
import net.nyxcraft.dev.kitpvp.util.ItemStackUtil;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public enum ProgressionItem implements Effect {

    /*
    #######################[ Primary Weapons ] *These are called Proficiencies*                    *Everything in A column is given to the player at lvl 0*
    (Allowed 1)                                 Wither 2 -
        Knockback 2 -   Poison 1 -  Wither 1 -  Poison 2 -                                      EXP earned per kill:
                                                                                                Less than Player LVL = 1
                                                                                                Equal than Player LVL = 1
    Iron Sword -    Fire Aspect 1 - Fire Aspect 2 - Demolitions Touch - Touch  2 -              Greater than Player LVL = 2
    ( + 2 Proficiencies)
    ( Iron Armor )
    Rage*   Sharpness 1 -   Sharpness 2 -
    
    *Rage is an example of...
    an ability* *Rage: Hold block for a 3 seconds > Explosion around player, and speed +2/resistance +2 for 10 seconds.   90 sec cooldown
        Demolitions Touch:  5% chance of explosion while striking an enemy.  +1% per level up.
        Posion Chance 4%    Posion Chance 8%    Nausea Chance 5%
    
    Rapid Bow - Wither Chance 4%    Wither Chance 8%    Slow Chance 8%
    ( + 1 Proficiencies )   *Chance: Effect for 2 seconds.
    ( Chain Armor ) *Barrage:  Charge bow all the way to release multiple arrows. No limit, but has to wait for arrows to respawn.  1 per second, max of 4
    Barrage*
        Punch 1 -   Punch 2 -   Punch 3 -                   GUI concept for building/selecting/resetting kits:  https://prtscr.us/u/061420360806.mp4
    
    Sharp Shooter   Power 2 -   Power 3 -   Power 4 -
    ( + 3 Proficiencies )
    ( Chain Armor/leather pants )   Flame 1 -   Flame 2 -   Flame 3 -
    Steady Mind*
        *Steady Mind: No knockback while bow is charging/charged.  Infinity
    #######################[ Secondary ]
    (Allowed 1)
    
    Gold Sword: Regenerate 1 -  Regenerate 2 -
    K . O .*    *K . O . : Sharpness 13, and breaks after one hit.
        Regenerate:  Sword respwns after 120 seconds.  -20 sec on level bump.
    Stone Axe:  Sharpness 1 -
    Thor's hammer*  Thor's hammer: Hitting a player causes a lighting strike.  Cooldown 65 seconds.
    ( + 1 Proficiency )
    
    Slingshot Bow:  Sharpness 1 -   Sharpness 2 -
    Slingshot*
    ( + 64 arrows  )    *Slingshot:  Left click to throw and arrow, after it hits and object, you get propelled forward towards it.
    ( + 1 Proficiency )
    
    Stone Sword:    Knockback 1 -   Knockback 2 -
    Shadow Step*
    ( + 1 Proficiency ) *Shadow Step:  Crouch + Block to teleport 10 blocks.
    
    Fishing Rod:    Regenerate 1 -  Regenerate 2 -
    Hook & snag*    *Hook & snag: Enemy get's pulled next to players after a susccessful hook, and then breaks.
    ( + 1 Proficiency ) Regenerate:  Rod respwns after 120 seconds.  -20 sec on level bump.
    */
    
    // Primary items
    ENRAGED_SWORD(Type.PRIMARY, new Rage(), equip(new ItemStack(Material.IRON_SWORD)), unequip(Material.IRON_SWORD)),
    RAPID_BOW(Type.PRIMARY, new Rapid(), equip(new ItemStack(Material.BOW)), unequip(Material.BOW)),
    SHARPSHOOTER_BOW(Type.PRIMARY, new Steady(), equip(new ItemStack(Material.BOW)), unequip(Material.BOW)),
    
    // Secondary items
    KNOCKOUT_SWORD(Type.SECONDARY, null, equip(ItemStackUtil.build(Material.GOLD_SWORD).withEnchant(Enchantment.DAMAGE_ALL, 13).withDurability(1).build()), unequip(Material.GOLD_SWORD)),
    THORS_HAMMER(Type.SECONDARY, new Charged(), equip(ItemStackUtil.build(Material.STONE_AXE).withEnchant(Enchantment.DAMAGE_ALL, 1).build()), unequip(Material.STONE_AXE)),
    SLINGSHOT_BOW(Type.SECONDARY, new Slingshot(), equip(new ItemStack(Material.BOW)), unequip(Material.BOW)),
    SHADOW_SWORD(Type.SECONDARY, new Blink(), equip(new ItemStack(Material.STONE_SWORD)), unequip(Material.STONE_SWORD)),
    HOOK_ROD(Type.SECONDARY, new Grapple(), equip(new ItemStack(Material.FISHING_ROD)), unequip(Material.FISHING_ROD));
    
    private final Consumer<Player> equip, unequip;
    private final Passive passive;
    private final Type type;
    private final Effect[][] proficiencies;
    
    private ProgressionItem(Type type, Passive passive, Consumer<Player> equip, Consumer<Player> unequip, Effect... effects) {
        this.type = type;
        this.passive = passive;
        this.equip = equip;
        this.unequip = unequip;
        this.proficiencies = wrap(effects, Effect[]::new, Effect[][]::new);
    }
    
    private ProgressionItem(Type type, Passive passive, Consumer<Player> equip, Consumer<Player> unequip, Effect[]... effects) {
        this.type = type;
        this.passive = passive;
        this.equip = equip;
        this.unequip = unequip;
        this.proficiencies = effects;
    }
    
    private ProgressionItem(Type type, Passive passive, Consumer<Player> equip, Consumer<Player> unequip) {
        this.type = type;
        this.passive = passive;
        this.equip = equip;
        this.unequip = unequip;
        this.proficiencies = new Effect[0][];
    }
    
    public Type getType() {
        return type;
    }
    
    public Effect[][] getProficiencies() {
        return proficiencies;
    }
    
    public Passive getPassive() {
        return passive;
    }
    
    public void equip(Player player) {
        ProgressionItem.handle(player, equip);
    }
    
    public void unequip(Player player) {
        ProgressionItem.handle(player, unequip);
    }
    
    
    static <T> void handle(T subject, Consumer<? super T> consumer) {
        if (consumer != null) {
            consumer.accept(subject);
        }
    }
    
    static <T, C> void handle(T subject, BiConsumer<? super C, ? super T> consumer, C coalesce) {
        if (consumer != null && coalesce != null) {
            consumer.accept(coalesce, subject);
        }
    }
    
    static <T> T[][] wrap(T[] array, IntFunction<T[]> one, IntFunction<T[][]> two) {
        T[][] base = two.apply(array.length);
        
        for (int i = 0; i < array.length; i++) {
            base[i] = one.apply(1);
            base[i][0] = array[i];
        }
        
        return base;
    }
    
    static Consumer<Player> equip(ItemStack item) {
        Objects.requireNonNull(item, "The given ItemStack to equip was null!");
        
        return p -> p.getInventory().addItem(item);
    }
    
    static Consumer<Player> set(ItemStack item, int index) {
        return p -> p.getInventory().setItem(index, item == null ? new ItemStack(Material.AIR) : item);
    }
    
    static Consumer<Player> unequip(ItemStack item) {
        return unequip(item, true);
    }
    
    static Consumer<Player> unequip(ItemStack item, boolean similar) {
        Objects.requireNonNull(item, "The given ItemStack to unequip was null!");
        
        return p -> {
            final Inventory inv = p.getInventory();
            
            for (int i = 0; i < inv.getSize(); i++) {
                if (item.isSimilar(inv.getItem(i))) {
                    inv.setItem(i, new ItemStack(Material.AIR));
                }
            }
        };
    }
    
    static Consumer<Player> unequip(Material material) {
        Objects.requireNonNull(material, "The given Material to remove was null!");
        
        return p -> {
            final Inventory inv = p.getInventory();
            
            for (int i = 0; i < inv.getSize(); i++) {
                final ItemStack slot = inv.getItem(i);
                final Material slotType;
                
                if (slot == null || (slotType = slot.getType()) == null) {
                    continue;
                } else if (material.equals(slotType)) {
                    inv.setItem(i, new ItemStack(Material.AIR));
                }
            }
        };
    }
    
    public static enum Boosts implements Effect {
    
        SPEED("Speed boost", 0, p -> p.addPotionEffect(PotionEffectType.SPEED.createEffect(Integer.MAX_VALUE, 0), true), p -> p.removePotionEffect(PotionEffectType.SPEED)),
        JUMP("Jump boost", 0, p -> p.addPotionEffect(PotionEffectType.JUMP.createEffect(Integer.MAX_VALUE, 0), true), p -> p.removePotionEffect(PotionEffectType.JUMP)),
        POISON_RESIST("Poison resistance", 10, null), // TODO needs a listener to cancel getting poisoned
        WITHER_RESIST("Wither resistance", 10, null), // TODO needs a listener to cancel getting withered
        FIRE_RESIST("Fire resistance", 15, p -> p.addPotionEffect(PotionEffectType.FIRE_RESISTANCE.createEffect(Integer.MAX_VALUE, 0), true), p -> p.removePotionEffect(PotionEffectType.FIRE_RESISTANCE)),
        THORNS_I("Thorns I", 15, null), // TODO needs an operation to enchant their existing armor with thorns (equip) and then remove it (unequip)
        THORNS_II("Thorns II", 20, null), // TODO "
        DOUBLE_JUMP("Double jump", 20, null), // TODO needs a listener to perform double jump
        ABSORPTION("Absorption", 25, null), // TODO needs a listener to absorb 4 health (2 hearts) when the player kills an enemy
        STRENGTH("Strength", 25, p -> p.addPotionEffect(PotionEffectType.INCREASE_DAMAGE.createEffect(Integer.MAX_VALUE, 0), true), p -> p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE)),
        REGEN("Regeneration", 30, p -> p.addPotionEffect(PotionEffectType.REGENERATION.createEffect(Integer.MAX_VALUE, 0), true), p -> p.removePotionEffect(PotionEffectType.REGENERATION)),
        RESISTANCE("Resistance", 30, p -> p.addPotionEffect(PotionEffectType.DAMAGE_RESISTANCE.createEffect(Integer.MAX_VALUE, 0), true), p -> p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE));
        
        private final String name;
        private final Predicate<GameManager.PlayerData> allow;
        private final Passive passive;
        
        private Boosts(String name, Predicate<GameManager.PlayerData> allow, Consumer<Player> equip, Consumer<Player> unequip) {
            this.name = name;
            this.allow = allow;
            this.passive = new ConcretePassive(equip, unequip);
        }
        
        private Boosts(String name, int level, Consumer<Player> equip, Consumer<Player> unequip) {
            this(name, pd -> pd.getPlayer().getLevel() >= level, equip, unequip);
        }
        
        private Boosts(String name, Predicate<GameManager.PlayerData> allow, Passive passive) {
            this.name = name;
            this.allow = allow;
            this.passive = passive;
        }
        
        private Boosts(String name, int level, Passive passive) {
            this(name, pd -> pd.getPlayer().getLevel() >= level, passive);
        }
        
        public String getDisplay() {
            return name;
        }
        
        public boolean check(Player player) {
            return check(GameManager.getInstance().getPlayerData(player));
        }
        
        public boolean check(GameManager.PlayerData pd) {
            return pd != null && pd.isValid() && allow.test(pd);
        }
        
        public void equip(Player player) {
            ProgressionItem.handle(player, Passive::equip, passive);
        }
        
        public void unequip(Player player) {
            ProgressionItem.handle(player, Passive::unequip, passive);
        }
    }
    
    public static enum Special implements Effect {
    
        GOLDEN_APPLE("Golden Apple", Material.GOLDEN_APPLE, ItemStackUtil.build(Material.GOLDEN_APPLE).build()),
        POTION_HEALTH("Restoration", Material.POTION, ItemStackUtil.build(Material.POTION).withDurability(8229).build()),
        POTION_INVISIBILITY("Invisibility", Material.POTION, ItemStackUtil.build(Material.POTION).withDurability(8270).build()),
        POTION_DAMAGE("Damage", Material.POTION, ItemStackUtil.build(Material.POTION).withDurability(16396).build()),
        POTION_POISON("Poison", Material.POTION, ItemStackUtil.build(Material.POTION).withDurability(16388).build()),
        TNT("Blast Charge", Material.TNT, ItemStackUtil.build(Material.TNT).build());
        
        private final String name;
        private final Consumer<Player> equip, unequip;
        
        private Special(String name, Material remove, ItemStack give) {
            this.name = name;
            this.equip = give == null ? null : ProgressionItem.equip(give);
            this.unequip = remove == null ? null : ProgressionItem.unequip(remove);
        }
        
        public String getDisplay() {
            return name;
        }
        
        public void equip(Player player) {
            ProgressionItem.handle(player, equip);
        }
        
        public void unequip(Player player) {
            ProgressionItem.handle(player, unequip);
        }
    }
}

final class ConcretePassive extends Passive {

    final Runnable stop;
    
    ConcretePassive(Consumer<Player> activate, Consumer<Player> deactivate, Runnable stop) {
        super(activate, deactivate);
        
        this.stop = stop;
    }
    
    ConcretePassive(Consumer<Player> activate, Consumer<Player> deactivate) {
        this(activate, deactivate, null);
    }
    
    public void stop() {
        if (stop != null) {
            stop.run();
        }
    }
}
