package net.nyxcraft.dev.kitpvp.game.config;

import java.io.File;
import java.util.Random;

import net.nyxcraft.dev.nyxutils.config.JsonConfig;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * Per-map configuration file template.
 * 
 * @author Gideon
 */
public class MapConfig extends JsonConfig {

    public static final String CONFIG_NAME = "config.json";
    private static final Random random = new Random();
    
    public Spawn[] spawns = {};
    public String name = "", author = "";
    public int seconds = 300;
    
    public Location getRandomSpawn(World world) {
        if (spawns != null && spawns.length > 0) {
            return spawns[random.nextInt(spawns.length)].toLocation(world);
        }
        
        return world.getSpawnLocation();
    }
    
    public String getName() {
        return name == null || name.isEmpty() ? "Unknown Map" : name;
    }
    
    public String getAuthor() {
        return author == null || name.isEmpty() ? "Unknown Author" : author;
    }
    
    
    public static MapConfig load(World world) {
        return load(new File(world.getWorldFolder(), CONFIG_NAME), MapConfig.class);
    }
    
    public static class Spawn {
    
        public double x, y, z;
        public float yaw;
        
        public Location toLocation(World world) {
            return new Location(world, x, y, z, yaw, 0f);
        }
    }
}
