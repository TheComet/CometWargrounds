package net.nyxcraft.dev.kitpvp.game;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import net.nyxcraft.dev.kitpvp.KitPVP;
import net.nyxcraft.dev.kitpvp.util.Items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.google.common.collect.Iterators;

/**
 * A class for representing a collection of {@link Items} that form a kit.
 * 
 * @author Gideon
 */
public class Kit {

    private static final String KIT_METADATA_IDENTIFIER = "equipped_kit_data";
    
    private final Items.Perks[] perks;
    private Items.Primary primary;
    private Items.Secondary secondary;
    private Items.Special special;
    
    public Kit(Items.Primary primary, Items.Secondary secondary, Items.Special special, Items.Perks... perks) {
        this.perks = Arrays.copyOfRange(perks == null ? new Items.Perks[0] : perks, 0, 3);
        this.primary = primary;
        this.secondary = secondary;
        this.special = special;
    }
    
    public Kit() {
        this(null, null, null);
    }
    
    public Kit clone() {
        return new Kit(primary, secondary, special, perks.clone());
    }
    
    public Kit setPrimary(Items.Primary primary) {
        this.primary = primary;
        return this;
    }
    
    public Kit setSecondary(Items.Secondary secondary) {
        this.secondary = secondary;
        return this;
    }
    
    public Kit setSpecial(Items.Special special) {
        this.special = special;
        return this;
    }
    
    public Kit addPerk(Items.Perks perk) {
        for (int i = 0; i < perks.length; i++) {
            if (perks[i] == null) {
                perks[i] = perk;
                return this;
            }
        }
        
        throw new IllegalStateException("This Kit already has all of its perk slots filled!");
    }
    
    public Kit setPerk(int perk_index, Items.Perks perk) {
        if (perk_index < 0 || perk_index >= perks.length) {
            throw new IllegalArgumentException("The given perk index is outside the range of this Kit!");
        }
        
        this.perks[perk_index] = perk;
        return this;
    }
    
    public Kit setPerks(Items.Perks... perks) {
        if (perks == null) {
            throw new IllegalArgumentException("The given Item.Perks[] was null!");
        }
        
        for (int i = 0; i < Math.min(perks.length, this.perks.length); i++) {
            this.perks[i] = perks[i];
        }
        
        return this;
    }
    
    public void equip(Player player) {
        Set<Items> items = new LinkedHashSet<>();
        items.add(primary);
        items.add(secondary);
        items.add(special);
        Iterators.forArray(perks).forEachRemaining(items::add);
        
        items.stream().filter(Objects::nonNull).forEachOrdered(i -> i.equip(player));
        setArmor(player);
        player.setMetadata(KIT_METADATA_IDENTIFIER, new EquippedKit(items));
        player.updateInventory();
    }
    
    
    public static boolean unequip(Player player) {
        Optional<EquippedKit> kit = player.getMetadata(KIT_METADATA_IDENTIFIER)
                .stream()
                .filter(EquippedKit.class::isInstance)
                .map(EquippedKit.class::cast)
                .findFirst();
        
        if (!kit.isPresent()) {
            return false;
        }
        
        kit.map(EquippedKit::value).get().stream().filter(Objects::nonNull).forEachOrdered(i -> i.unequip(player));
        clearArmor(player);
        return true;
    }
    
    public static void generateRandomKit(Player player) {
        GameManager.PlayerData data = GameManager.getInstance().getPlayerData(player);
        
        if (data == null) {
            return;
        }
        
        Kit random = new Kit();
        random.setPrimary(Items.random(Items.Primary.class));
        random.setSecondary(Items.random(Items.Secondary.class));
        random.setSpecial(Items.random(Items.Special.class));
        random.setPerks(Items.random(Items.Perks.class), Items.random(Items.Perks.class), Items.random(Items.Perks.class));
        data.setKit(random);
    }
    
    public static void setArmor(Player player) {
        player.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
        player.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
        player.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
        player.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
    }
    
    public static void clearArmor(Player player) {
        player.getEquipment().setHelmet(new ItemStack(Material.AIR));
        player.getEquipment().setChestplate(new ItemStack(Material.AIR));
        player.getEquipment().setLeggings(new ItemStack(Material.AIR));
        player.getEquipment().setBoots(new ItemStack(Material.AIR));
    }
    
    public static class EquippedKit extends FixedMetadataValue {
    
        private EquippedKit(Set<Items> items) {
            super(KitPVP.getInstance(), items);
        }
        
        @SuppressWarnings("unchecked")
        public Set<Items> value() {
            Object items = super.value();
            
            if (items instanceof Set) {
                return (Set<Items>) super.value();
            } else if (items == null) {
                return new LinkedHashSet<>();
            }
            
            throw new IllegalStateException("This EquippedKit metadata does not have a valid item Set underlying it!");
        }
    }
}
