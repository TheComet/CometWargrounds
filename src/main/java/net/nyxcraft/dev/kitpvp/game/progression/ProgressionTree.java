package net.nyxcraft.dev.kitpvp.game.progression;

import java.util.EnumMap;
import java.util.Objects;

import net.nyxcraft.dev.kitpvp.game.GameManager;

public class ProgressionTree {

    private final GameManager.PlayerData data;
    private final EnumMap<ProgressionItem, Node> nodes;
    
    public ProgressionTree(GameManager.PlayerData pd) {
        this.data = pd;
        this.nodes = new EnumMap<>(ProgressionItem.class);
    }
    
    public GameManager.PlayerData getPlayer() {
        return data;
    }
    
    public Node of(ProgressionItem item) {
        Node node = nodes.get(Objects.requireNonNull(item, "The given ProgressionItem was null!"));
        
        if (node == null) {
            node = initialize(item);
        }
        
        return Objects.requireNonNull(node, "No Node was present for the given ProgressionItem!");
    }
    
    public Node initialize(ProgressionItem item) {
        Effect[][] prof = item.getProficiencies();
        
        if (prof.length < 1) {
            return new Node();
        }
        
        Node[] roots = new Node[prof[0].length];
        
        // TODO Populate Node[] with Nodes representing the ProgressionItem's Effect[][]
//        for (int i = 0; i < prof.length; i++) {
//            Node[] branches = new Node[prof[i].length];
//            
//            for (int j = 0; j < prof[i].length; j++) {
//                
//            }
//        }
        
        return new Node(roots);
    }
    
    public class Node {

        private final Node[] children;
        private final Effect effect;
        
        Node(Effect effect, Node... children) {
            this.children = children;
            this.effect = effect;
        }
        
        Node(Node... children) {
            this.children = children;
            this.effect = null;
        }
        
        Node(Effect effect) {
            this.children = null;
            this.effect = effect;
        }
        
        Node() {
            this.children = null;
            this.effect = null;
        }
        
        public boolean isBranch() {
            return children != null;
        }
        
        public boolean isLeaf() {
            return effect != null;
        }
        
        public Node[] getChildren() {
            return children.clone();
        }
        
        public Effect getEffect() {
            return effect;
        }
    }
}
