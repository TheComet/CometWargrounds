package net.nyxcraft.dev.kitpvp.game;

/**
 * An Enum representing states that a player may be in.
 * 
 * @author Gideon
 */
public enum PlayerState {

    FIGHTING,
    SPECTATING;

    private PlayerState() {
        
    }
}
