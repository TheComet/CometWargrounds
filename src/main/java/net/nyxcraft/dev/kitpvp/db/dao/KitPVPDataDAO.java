package net.nyxcraft.dev.kitpvp.db.dao;

import net.nyxcraft.dev.kitpvp.db.entity.KitPVPData;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class KitPVPDataDAO extends BasicDAO<KitPVPData, ObjectId> {

    private static KitPVPDataDAO instance = null;
    
    public KitPVPDataDAO(Datastore datastore) {
        super(datastore);
        
        if (KitPVPDataDAO.instance != null) {
            throw new IllegalStateException("The KitPVPDataDAO has already been initialized!");
        }
        
        KitPVPDataDAO.instance = this;
    }
    
    public KitPVPData initData() {
        KitPVPData data = find().get();
        
        if (data == null) {
            data = new KitPVPData();
            save(data);
        }
        
        return data;
    }
    
    
    public static KitPVPDataDAO getInstance() {
        return instance;
    }
}
