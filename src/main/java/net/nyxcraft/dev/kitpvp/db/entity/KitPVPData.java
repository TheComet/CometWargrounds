package net.nyxcraft.dev.kitpvp.db.entity;

import java.util.Map;

import org.bson.types.ObjectId;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "game_kitpvp_data", noClassnameStored = true)
public class KitPVPData {

    @Id
    public ObjectId id;
    public Map<String, PlayerScore> scores;
    
    public PlayerScore applyScore(Player player, PlayerScore score) {
        String uuidstring = player.getUniqueId().toString();
        return scores.compute(uuidstring, (key, present) -> present == null ? score : present.delta(score));
    }
    
    
    public static class PlayerScore {
    
        public int kills;
        public int deaths;
        
        public PlayerScore(int kills, int deaths) {
            this.kills = kills;
            this.deaths = deaths;
        }
        
        public PlayerScore() {
            this(0, 0);
        }
        
        public PlayerScore delta(PlayerScore other) {
            if (other != null) {
                this.kills += other.kills;
                this.deaths += other.deaths;
            }
            
            return this;
        }
    }
}
